package com.hcl.retailbanking.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.retailbanking.dto.BeneficiaryDto;
import com.hcl.retailbanking.dto.ResponseBeneficiaryDto;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.entity.Customer;
import com.hcl.retailbanking.exception.BeneficiaryException;
import com.hcl.retailbanking.repository.BeneficiaryRepository;
import com.hcl.retailbanking.repository.CustomerRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BeneficiaryServiceImplTest {

	@InjectMocks
	BeneficiaryServiceImpl beneficiaryServiceImpl;
	@Mock
	BeneficiaryRepository beneficiaryRespository;
	@Mock
	CustomerRepository customerRepository;

	Beneficiary beneficiary;
	List<Beneficiary> beneficiaryList;
	Customer customer;
	BeneficiaryDto beneficiaryDto;
	List<BeneficiaryDto> beneficiaryDtoList;
	ResponseBeneficiaryDto responseBeneficiaryDto;

	@Before
	public void setUp() {
		beneficiary = new Beneficiary();
		beneficiary.setBeneficiaryId(1);
		beneficiary.setCustomerId(1);
		beneficiary.setBeneficiaryName("Krishna");
		beneficiary.setIfscCode("SBI0045");
		beneficiary.setAccountNumber(1234L);
		beneficiary.setBalance(12345.00);
		
		beneficiaryList = new ArrayList<>();
		beneficiaryList.add(beneficiary);

		customer = new Customer();
		customer.setUserId(1L);
		customer.setPassword("anu");
		customer.setCustomerName("Anuradha");
		customer.setBeneficiarydetails(beneficiaryList);
		
		beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setBeneficiarydetails(beneficiaryList);
		beneficiaryDto.setCustomerId(1);
		
		beneficiaryDtoList = new ArrayList<>();
		
		responseBeneficiaryDto = new ResponseBeneficiaryDto();
		responseBeneficiaryDto.setBeneficiarydto(beneficiaryDtoList);
		responseBeneficiaryDto.setMessage("Success");
		responseBeneficiaryDto.setStatusCode(607);
	}

	@Test
	public void getBeneficiarydetailsTest() throws BeneficiaryException {
		Mockito.when(customerRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(customer));
		List<Beneficiary> actual = beneficiaryServiceImpl.getbeneficiarydetails(1);
		assertNotNull(actual);
	}
	
	@Test(expected = BeneficiaryException.class)
	public void getBeneficiarydetailsTest1() throws BeneficiaryException {
		Mockito.when(customerRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		beneficiaryServiceImpl.getbeneficiarydetails(1);
	}
	
	@Test
	public void addBeneficiary() throws BeneficiaryException {
		Mockito.when(customerRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(customer));
		beneficiaryServiceImpl.addBeneficiaryService(beneficiaryDto);
	}
	
	@Test(expected = BeneficiaryException.class)
	public void addBeneficiary1() throws BeneficiaryException {
		Mockito.when(customerRepository.findById(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		beneficiaryServiceImpl.addBeneficiaryService(beneficiaryDto);
	}
}
