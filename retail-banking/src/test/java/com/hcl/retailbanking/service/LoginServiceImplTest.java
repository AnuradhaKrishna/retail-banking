package com.hcl.retailbanking.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.retailbanking.dto.LoginDto;
import com.hcl.retailbanking.dto.ResponseDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Customer;
import com.hcl.retailbanking.exception.CustomisedException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.CustomerRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	AccountRepository accountRepository;

	LoginDto loginDto;;
	ResponseDto responseDto;
	Customer customer;
	List<Customer> customerList;
	Account account;
	List<Account> accountList;

	@Before
	public void setUp() {
		loginDto = new LoginDto();
		loginDto.setPassword("123");
		loginDto.setUserId(1L);

		responseDto = new ResponseDto();
		responseDto.setAccountNumber(1234455L);
		responseDto.setBalance(12000D);
		responseDto.setCustomerName("Baiu");
		responseDto.setStatusCode(607);

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setCustomerName("Baiu");
		customer.setPassword("123");
		customer.setUserId(1L);

		customerList = new ArrayList<>();
		customerList.add(customer);

		account = new Account();
		account.setAccountId(12345);
		account.setAccountNumber(12355L);
		account.setBalance(12341D);
		account.setCustomerId(1);

		accountList = new ArrayList<Account>();
		accountList.add(account);

	}

	@Test
	public void loginTest() throws CustomisedException {
		Mockito.when(customerRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.of(customerList));
		Mockito.when(accountRepository.findByCustomerId(Mockito.anyInt())).thenReturn(Optional.of(accountList));
		ResponseDto result = loginServiceImpl.userLogin(loginDto);
		Assert.assertNotNull(result);
		assertEquals(607, result.getStatusCode());
		assertEquals("Baiu", result.getCustomerName());
	}

	@Test(expected = CustomisedException.class)
	public void loginTest1() throws CustomisedException {
		Mockito.when(customerRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.ofNullable(null));
		loginServiceImpl.userLogin(loginDto);

	}

	@Test(expected = CustomisedException.class)
	public void loginTest2() throws CustomisedException {
		account = new Account();
		account.setAccountId(12345);
		account.setAccountNumber(12355L);
		account.setBalance(12341D);
		account.setCustomerId(1);

		accountList = new ArrayList<Account>();
		accountList.add(account);
		Mockito.when(customerRepository.findByUserIdAndPassword(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(Optional.of(customerList));
		Mockito.when(accountRepository.findByCustomerId(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		loginServiceImpl.userLogin(loginDto);
	}

}
