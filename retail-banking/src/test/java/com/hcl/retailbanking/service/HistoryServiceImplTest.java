package com.hcl.retailbanking.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.retailbanking.dto.HistoryDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Transaction;
import com.hcl.retailbanking.exception.TransactionHistoryException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HistoryServiceImplTest {

	@InjectMocks
	HistoryServiceImpl historyServiceImpl;
	@Mock
	TransactionRepository transactionRepository;
	@Mock
	AccountRepository accountRepository;
	Account account;
	List<Account> accountList;

	Transaction transaction;
	List<Transaction> transactionList;
	HistoryDto historyDto;
	HistoryDto historyDto1;
	List<HistoryDto> historyDtoList;

	@Before
	public void setup() {
		account = new Account();
		account.setAccountId(123);
		account.setAccountNumber(12345L);
		account.setBalance(2000D);
		account.setCustomerId(1);

		accountList = new ArrayList<>();
		accountList.add(account);

		transaction = new Transaction();
		transaction.setAmount(300D);
		transaction.setDateTime(LocalDateTime.now());
		transaction.setDescription("Transaction Successful!!!");
		transaction.setFromAccount(12345L);
		transaction.setToAccount(56748L);
		transaction.setTransactionType("DEBIT");
		transaction.setTransactionId(1);

		transactionList = new ArrayList<>();
		transactionList.add(transaction);

		historyDto = new HistoryDto();
		historyDto.setAmount(300D);
		historyDto.setDateTime(LocalDateTime.now());
		historyDto.setDescription("Transaction Successful!!!");
		historyDto.setFromAccount(12345L);
		historyDto.setToAccount(56748L);
		historyDto.setTransactionType("DEBIT");

		historyDtoList = new ArrayList<>();
		historyDtoList.add(historyDto);
		historyDtoList.add(historyDto1);

	}

	@Test
	public void historyTest() throws TransactionHistoryException {
		Mockito.when(accountRepository.findByCustomerId(Mockito.anyInt())).thenReturn(Optional.of(accountList));
		Mockito.when(transactionRepository.findByFromAccount(Mockito.anyLong()))
				.thenReturn(Optional.of(transactionList));
		List<HistoryDto> response = historyServiceImpl.history(1);
		Assert.assertNotNull(response);
		Assert.assertEquals("DEBIT", response.get(0).getTransactionType());
		Assert.assertEquals("Transaction Successful!!!", response.get(0).getDescription());
	}

	@Test(expected = TransactionHistoryException.class)
	public void historyNoUserTest1() throws TransactionHistoryException {
		Mockito.when(accountRepository.findByCustomerId(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		List<HistoryDto> actual = historyServiceImpl.history(1);
		Assert.assertEquals(historyDtoList, actual);
	}
}
