package com.hcl.retailbanking.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.retailbanking.constants.ApplicationConstants;
import com.hcl.retailbanking.dto.FundTransferRequestDto;
import com.hcl.retailbanking.dto.FundTransferResponseDto;
import com.hcl.retailbanking.dto.TransactionDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.exception.BalanceNotSufficientException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.BeneficiaryRepository;
import com.hcl.retailbanking.repository.CustomerRepository;
import com.hcl.retailbanking.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FundTransferServiceTest {
	@InjectMocks
	FundTransferServiceImpl fundTransferServiceImpl;
	@Mock
	AccountRepository accountRepository;
	@Mock
	BeneficiaryRepository beneficiaryRepository;
	@Mock
	TransactionRepository transactionHistoryRepository;
	@Mock
	CustomerRepository customerRepository;
	
	TransactionDto transactionDto;
	FundTransferRequestDto fundTransferRequestDto;
	Account account1;
	Optional<Beneficiary> beneficiary1;
	FundTransferResponseDto fundTransferResponseDto;

	Beneficiary beneficiary;

	@Before
	public void before() {
		transactionDto = new TransactionDto();
		transactionDto.setAmount(100);
		transactionDto.setDateTime(LocalDateTime.now());
		transactionDto.setDescription(ApplicationConstants.DESCRIPTION);
		transactionDto.setFromAccount(121212L);
		transactionDto.setToAccount(141414L);
		transactionDto.setTransactionType("DEBIT");

		account1 = new Account();
		account1.setAccountId(1);
		account1.setAccountNumber((long) 121212);
		account1.setBalance(1000D);
		account1.setCustomerId(1);

		beneficiary = new Beneficiary();
		beneficiary.setAccountNumber(141414L);
		beneficiary.setBalance(2000D);
		beneficiary.setBeneficiaryId(1);
		beneficiary.setBeneficiaryName("suma");
		beneficiary.setCustomerId(1);
		beneficiary.setIfscCode("000452IC");

		fundTransferRequestDto = new FundTransferRequestDto();
		fundTransferRequestDto.setAmount(100);
		fundTransferRequestDto.setDate(LocalDateTime.now());
		fundTransferRequestDto.setFromAccount(121212L);
		fundTransferRequestDto.setToAccount(141414L);
		fundTransferRequestDto.setCustomerId(1);

		fundTransferResponseDto = new FundTransferResponseDto();
		fundTransferResponseDto.setMessage("Success");
		fundTransferResponseDto.setStatusCode(607);

	}
	
	@Test
	public void fundTransferTests() throws BalanceNotSufficientException {
		Mockito.when(accountRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
		.thenReturn(account1);
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyLong())).thenReturn((Optional.of(account1)));
		Mockito.when(beneficiaryRepository.findByAccountNumber(Mockito.anyLong()))
				.thenReturn((Optional.of(beneficiary)));		
		Mockito.when(beneficiaryRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
				.thenReturn(beneficiary);		
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);

	}
	@Test
	public void fundTransferTestsNegative() throws BalanceNotSufficientException {
		account1 = new Account();
		account1.setBalance(3000D);
		account1.setAccountId(1);
		account1.setAccountNumber((long) 121212);
		account1.setCustomerId(1);
		fundTransferRequestDto = new FundTransferRequestDto();
		fundTransferRequestDto.setAmount(5000D);
		fundTransferRequestDto.setDate(LocalDateTime.now());
		fundTransferRequestDto.setFromAccount(121212L);
		fundTransferRequestDto.setToAccount(141414L);
		fundTransferRequestDto.setCustomerId(1);

		Mockito.when(accountRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
		.thenReturn(account1);
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyLong())).thenReturn((Optional.of(account1)));
		Mockito.when(beneficiaryRepository.findByAccountNumber(Mockito.anyLong()))
				.thenReturn((Optional.of(beneficiary)));		
		Mockito.when(beneficiaryRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
				.thenReturn(beneficiary);		
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);		
	}

	@Test(expected = BalanceNotSufficientException.class)
	public void fundTransferTest() throws BalanceNotSufficientException {
		Mockito.when(accountRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
		.thenReturn(account1);
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);
	}
	
	@Test(expected = BalanceNotSufficientException.class)
	public void fundTransferTest2() throws BalanceNotSufficientException {
		Mockito.when(accountRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
		.thenReturn(null);
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);
	}
	@Test(expected = BalanceNotSufficientException.class)
	public void fundTransferTest3() throws BalanceNotSufficientException {
		Mockito.when(beneficiaryRepository.findByCustomerIdAndAccountNumber(Mockito.anyInt(), Mockito.anyLong()))
		.thenReturn(null);
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);
	}
	@Test(expected = BalanceNotSufficientException.class)
	public void fundTransferTest4() throws BalanceNotSufficientException {
		Mockito.when(beneficiaryRepository.findByAccountNumber(Mockito.anyLong()))
		.thenReturn((Optional.ofNullable(null)));
		fundTransferServiceImpl.transferAmount(fundTransferRequestDto);
	}
	
	

}