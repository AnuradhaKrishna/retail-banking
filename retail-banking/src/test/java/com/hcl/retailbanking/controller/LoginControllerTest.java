package com.hcl.retailbanking.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.retailbanking.dto.LoginDto;
import com.hcl.retailbanking.dto.ResponseDto;
import com.hcl.retailbanking.exception.CustomisedException;
import com.hcl.retailbanking.service.LoginService;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;

	@Mock
	LoginService loginService;

	@Test
	public void userLoginTest()  throws CustomisedException{
		LoginDto loginDto = new LoginDto();
		loginDto.setPassword("123");
		loginDto.setUserId(1L);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setAccountNumber(1234455L);
		responseDto.setBalance(12000D);
		responseDto.setCustomerName("Baiu");
		responseDto.setStatusCode(200);

		Mockito.when(loginService.userLogin(loginDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = loginController.userLogin(loginDto);
		Assert.assertNotNull(result);
		assertEquals(HttpStatus.OK,result.getStatusCode());

	}
}
