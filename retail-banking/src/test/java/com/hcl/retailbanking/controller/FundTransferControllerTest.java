package com.hcl.retailbanking.controller;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.retailbanking.dto.FundTransferRequestDto;
import com.hcl.retailbanking.dto.FundTransferResponseDto;
import com.hcl.retailbanking.exception.BalanceNotSufficientException;
import com.hcl.retailbanking.service.FundTransferService;
@RunWith(MockitoJUnitRunner.class)
public class FundTransferControllerTest {

	@InjectMocks
	TransactionController fundTransferController;
	@Mock
	FundTransferService fundTransferService;

	@Test
	public void fundTransferTest() throws BalanceNotSufficientException {
		FundTransferRequestDto fundTransferRequestDto = new FundTransferRequestDto();
		fundTransferRequestDto.setAmount(100);
		fundTransferRequestDto.setCustomerId(1);
		fundTransferRequestDto.setDate(LocalDateTime.now());
		fundTransferRequestDto.setFromAccount(121212L);
		fundTransferRequestDto.setToAccount(141414L);

		FundTransferResponseDto fundTransferResponseDto = new FundTransferResponseDto();
		fundTransferResponseDto.setStatusCode(200);
	
		Mockito.when(fundTransferService.transferAmount(fundTransferRequestDto)).thenReturn(fundTransferResponseDto);
		ResponseEntity<FundTransferResponseDto> result = fundTransferController.transferFund(fundTransferRequestDto);
		Assert.assertNotNull(result);
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}
}