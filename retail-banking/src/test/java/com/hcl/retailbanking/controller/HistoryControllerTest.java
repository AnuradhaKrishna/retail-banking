package com.hcl.retailbanking.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.retailbanking.dto.HistoryDto;
import com.hcl.retailbanking.exception.TransactionHistoryException;
import com.hcl.retailbanking.service.HistoryService;

@RunWith(MockitoJUnitRunner.class)
public class HistoryControllerTest {
	
	@InjectMocks
	HistoryController historyController;
	
	@Mock
	HistoryService historyService;
	
	@Test
	public void historyTest() throws TransactionHistoryException {
		HistoryDto historyDto=new HistoryDto();
		historyDto.setAmount(12000D);
		historyDto.setFromAccount(123445L);
		historyDto.setToAccount(446777L);
		historyDto.setDescription("Success");
		
		List<HistoryDto> hist=new ArrayList<HistoryDto>();
		hist.add(historyDto);
		
		Mockito.when(historyService.history(Mockito.anyInt())).thenReturn(hist);		
		ResponseEntity<List<HistoryDto>> result=historyController.history(1);		
		Assert.assertNotNull(result);		
		assertEquals(HttpStatus.OK, result.getStatusCode());
		
		
	}
	
}
