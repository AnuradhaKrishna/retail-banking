package com.hcl.retailbanking.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.retailbanking.dto.BeneficiaryDto;
import com.hcl.retailbanking.dto.ResponseBeneficiaryDto;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.exception.BeneficiaryException;
import com.hcl.retailbanking.service.BeneficiaryService;

@RunWith(MockitoJUnitRunner.class)
public class BeneficiaryControllerTest {
	@InjectMocks
	BeneficiaryController beneficiaryController;

	@Mock
	BeneficiaryService beneficiaryService;

	@Test
	public void getbeneficiarydetails() throws BeneficiaryException {
		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setAccountNumber(12344L);
		beneficiary.setBeneficiaryId(1);
		beneficiary.setBeneficiaryName("Haran");
		beneficiary.setIfscCode("1233");
		List<Beneficiary> result = new ArrayList<>();
		result.add(beneficiary);

		Mockito.when(beneficiaryService.getbeneficiarydetails(1)).thenReturn(result);
		ResponseEntity<List<Beneficiary>> result1 = beneficiaryController.getbeneficiarydetails(1);
		assertNotNull(result1);
		assertEquals(HttpStatus.OK, result1.getStatusCode());
	}
	
	BeneficiaryDto beneficiaryDto;
	List<BeneficiaryDto> beneficiaryDtoList;
	List<Beneficiary> beneficiaryList;
	
	@Test
	public void addBeneficiaryTest() throws BeneficiaryException {
		Beneficiary beneficiary = new Beneficiary();
		beneficiary.setAccountNumber(12344L);
		beneficiary.setBeneficiaryId(1);
		beneficiary.setBeneficiaryName("Haran");
		beneficiary.setIfscCode("1233");

		beneficiaryDtoList = new ArrayList<>();
		beneficiaryDtoList.add(beneficiaryDto);
		
		beneficiaryDto = new BeneficiaryDto();
		beneficiaryDto.setCustomerId(1);
		beneficiaryDto.setBeneficiarydetails(beneficiaryList);
		
		
		beneficiaryList = new ArrayList<>();
		beneficiaryList.add(beneficiary);		
		
		ResponseBeneficiaryDto responseBeneficiaryDto = new ResponseBeneficiaryDto();
		responseBeneficiaryDto.setMessage("Added Successfully!!!");
		responseBeneficiaryDto.setStatusCode(200);
		responseBeneficiaryDto.setBeneficiarydto(beneficiaryDtoList);
		
		Mockito.when(beneficiaryService.addBeneficiaryService(beneficiaryDto)).thenReturn(responseBeneficiaryDto);	
		ResponseEntity<ResponseBeneficiaryDto> result1 = beneficiaryController.addBeneficiary(beneficiaryDto);
		assertNotNull(result1);
		assertEquals(HttpStatus.OK, result1.getStatusCode());
		
	}
}
