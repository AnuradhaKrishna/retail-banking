package com.hcl.retailbanking.constants;

public class ApplicationConstants {
	
	private ApplicationConstants() {
		
	}

	public static final String FILL_CREDENTIALS = "Please Fill the Credentials";
	public static final String ENTER_VALID_DETAILS = "Please Enter Valid Credentials";
	public static final String NOT_REGISTERED = "Beceficiary Not Registered";
	public static final String PROVIDE_COMPLETE_DETAILS = "Please fill Mandatory details";
	public static final String REGISTER_SUCCESS_MESSAGE = "Beneficiary Registered Successfully";
	public static final String CUSTOMER_NOT_REGISTERED = "Customer Not Available";
	public static final String PROVIDE_BENEFICIARY_DETAILS = "Please provide beneficiary details";
	public static final Integer SUCCESS_CODE = 607;
	public static final Integer UNSUCCESS_CODE = 608;
	public static final Integer NOT_FOUND = 604;
	public static final String NOUSER_FOUND = "No user available";
	public static final String ACCOUNT_NOT_PRESENT = "Account number not available";
	public static final String STATUS = "payment Succesful";
    public static final String DESCRIPTION   = "THE REQUESTED AMOUNT IS TRANSFERRED TO BENEFICIARY ACCOUNT";
    public static final String BALANCE_INSUFFICIENT = "ACCOUNT BALANCE INSUFFICIENT";
    public static final String CUSTOMER_NOT_FOUND = "SENDER ACCOUNT DETAILS ARE INCORRECT";
    public static final String RECEIVER_NOT_FOUND = "RECEIVER ACCOUNT DETAILS ARE INCORRECT";
    public static final String TRANSACTION_TYPE = "DEBIT";
    public static final String MESSAGE = "TRANSACTION COMPLETED";
    public static final String FAILURE_MESSAGE ="TRANSACTION INCOMPLETE!!! NOT ENOUGH BALANCE";
    public static final String FAILURE_STATUS = "Payment Unsuccessful";
    public static final String FAILURE_DESCRIPTION = "THE REQUESTED AMOUNT CANNOT BE TRANSFERRED";
	public static final String INVALID_DETAILS = "Please enter valid details";
}
