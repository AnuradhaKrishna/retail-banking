package com.hcl.retailbanking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.retailbanking.dto.BeneficiaryDto;
import com.hcl.retailbanking.dto.ResponseBeneficiaryDto;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.exception.BeneficiaryException;
import com.hcl.retailbanking.service.BeneficiaryService;
@RestController
@RequestMapping("/retail-banking")
public class BeneficiaryController {

	@Autowired
	BeneficiaryService beneficiaryService;

	@PostMapping("/beneficiary")
	public ResponseEntity<ResponseBeneficiaryDto> addBeneficiary(@RequestBody BeneficiaryDto beneficiarydto) throws BeneficiaryException {
		ResponseBeneficiaryDto responseDto = beneficiaryService.addBeneficiaryService(beneficiarydto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@GetMapping("/beneficiaries/{customerId}")
	public ResponseEntity<List<Beneficiary>> getbeneficiarydetails(@RequestParam("customerId") int customerId)
			throws BeneficiaryException {
		List<Beneficiary> responseDto = beneficiaryService.getbeneficiarydetails(customerId);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

}
