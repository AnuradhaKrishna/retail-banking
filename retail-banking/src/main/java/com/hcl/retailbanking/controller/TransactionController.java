package com.hcl.retailbanking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.retailbanking.dto.FundTransferRequestDto;
import com.hcl.retailbanking.dto.FundTransferResponseDto;
import com.hcl.retailbanking.exception.BalanceNotSufficientException;
import com.hcl.retailbanking.service.FundTransferService;

@RestController
@RequestMapping("/retail-banking")
public class TransactionController {

	@Autowired
	FundTransferService fundTransferService;

	@PostMapping("/fundTransfer")
	public ResponseEntity<FundTransferResponseDto> transferFund(@RequestBody FundTransferRequestDto fundTransferRequestDto)
			throws BalanceNotSufficientException {
		return new ResponseEntity<>(fundTransferService.transferAmount(fundTransferRequestDto), HttpStatus.OK);

	}

}
