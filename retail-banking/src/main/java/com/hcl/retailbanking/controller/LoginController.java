package com.hcl.retailbanking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.retailbanking.dto.LoginDto;
import com.hcl.retailbanking.dto.ResponseDto;
import com.hcl.retailbanking.exception.CustomisedException;
import com.hcl.retailbanking.service.LoginService;

@RestController
@RequestMapping("/retail-banking")
public class LoginController {

	@Autowired
	LoginService loginService;

	/**
	 * This method is used to check the login details of the user
	 * 
	 * @author Uma
	 * @since 2020-03-12
	 * @param emailId  -Here we use emailId to check the username correct or not
	 * @param password -Here we use password to check the password correct or not
	 * @return ResponseEntity Object along with status code and success login
	 *         message
	 * @throws CustomisedException 
	 */

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@RequestBody LoginDto loginDto) throws CustomisedException{
		ResponseDto responseDto = loginService.userLogin(loginDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

}
