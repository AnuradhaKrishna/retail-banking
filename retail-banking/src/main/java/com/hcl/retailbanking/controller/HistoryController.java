package com.hcl.retailbanking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.retailbanking.dto.HistoryDto;
import com.hcl.retailbanking.exception.TransactionHistoryException;
import com.hcl.retailbanking.service.HistoryService;

@RestController
@RequestMapping("/retail-banking")
public class HistoryController {
	
	@Autowired
	HistoryService historyService;
	
	@GetMapping("/transaction/{userId}")
	public ResponseEntity<List<HistoryDto>> history(@PathVariable("userId") Integer userId) throws TransactionHistoryException{
		return new ResponseEntity<>(historyService.history(userId), HttpStatus.OK);
	}
	

}
