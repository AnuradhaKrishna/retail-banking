package com.hcl.retailbanking.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer customerId;
	private Long userId;
	private String password;
	private String customerName;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId")
	List<Beneficiary> beneficiarydetails;

}
