package com.hcl.retailbanking.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.retailbanking.constants.ApplicationConstants;
import com.hcl.retailbanking.dto.BeneficiaryDto;
import com.hcl.retailbanking.dto.ResponseBeneficiaryDto;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.entity.Customer;
import com.hcl.retailbanking.exception.BeneficiaryException;
import com.hcl.retailbanking.repository.BeneficiaryRepository;
import com.hcl.retailbanking.repository.CustomerRepository;

@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {

	@Autowired
	BeneficiaryRepository beneficiaryRespository;

	@Autowired
	CustomerRepository customerRepository;
	/**
	 * @author Haran
	 * 
	 *         Method is used adding beneficiary using beneficiarydto that includes its details
	 *          i.e, beneficiary name,account no,balance n customerId
	 * 
	 * @param beneficiarydto
	 * @return ResponseBeneficiaryDto that includes statusCode, message
	 *  
	 * @throws BeneficiaryException
	 */
	@Override
	public ResponseBeneficiaryDto addBeneficiaryService(BeneficiaryDto beneficiarydto) throws BeneficiaryException {
		ResponseBeneficiaryDto responseDto = new ResponseBeneficiaryDto();
		Optional<Customer> customer = customerRepository.findById(beneficiarydto.getCustomerId());
		if(!customer.isPresent()) {
			throw new BeneficiaryException(ApplicationConstants.NOUSER_FOUND);
		}
		List<Beneficiary> beneficiarydetails = beneficiarydto.getBeneficiarydetails();
		for (Beneficiary object : beneficiarydetails) {
			object.setCustomerId(customer.get().getCustomerId());
			beneficiaryRespository.save(object);
			responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
			responseDto.setMessage("Beneficiary Registered SuccessFully");
		}
		return responseDto;
	}

	/**
	 * @author Haran
	 * 
	 *         Method is used fetching beneficiary details based on customerId
	 *          i.e, beneficiary name,account no,balance n customerId
	 * 
	 * @param customerId
	 * @return List<Beneficiary> that includes details of beneficiary
	 *  
	 * @throws BeneficiaryException
	 */
	@Override
	public List<Beneficiary> getbeneficiarydetails(int customerId) throws BeneficiaryException {
		List<Beneficiary> list = null;
		Optional<Customer> customer = customerRepository.findById(customerId);
		if (!customer.isPresent()) {
			throw new BeneficiaryException(ApplicationConstants.CUSTOMER_NOT_REGISTERED);
		}
		list = customer.get().getBeneficiarydetails();
		return list;

	}

}
