package com.hcl.retailbanking.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.retailbanking.dto.HistoryDto;
import com.hcl.retailbanking.exception.TransactionHistoryException;

@Service
public interface HistoryService {
	
	public List<HistoryDto> history(Integer userId) throws TransactionHistoryException;

}
