package com.hcl.retailbanking.service;

import org.springframework.stereotype.Service;

import com.hcl.retailbanking.dto.LoginDto;
import com.hcl.retailbanking.dto.ResponseDto;
import com.hcl.retailbanking.exception.CustomisedException;
@Service
public interface LoginService {

	public ResponseDto userLogin(LoginDto loginDto) throws CustomisedException;

}
