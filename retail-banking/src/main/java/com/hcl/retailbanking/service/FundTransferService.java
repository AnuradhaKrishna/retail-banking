package com.hcl.retailbanking.service;

import org.springframework.stereotype.Service;

import com.hcl.retailbanking.dto.FundTransferRequestDto;
import com.hcl.retailbanking.dto.FundTransferResponseDto;
import com.hcl.retailbanking.exception.BalanceNotSufficientException;

@Service
public interface FundTransferService {
	
	public FundTransferResponseDto transferAmount(FundTransferRequestDto fundTransferRequestDto) throws BalanceNotSufficientException;

}
