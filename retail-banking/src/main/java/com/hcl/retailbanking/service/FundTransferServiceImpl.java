package com.hcl.retailbanking.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.retailbanking.constants.ApplicationConstants;
import com.hcl.retailbanking.dto.FundTransferRequestDto;
import com.hcl.retailbanking.dto.FundTransferResponseDto;
import com.hcl.retailbanking.dto.TransactionDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.entity.Transaction;
import com.hcl.retailbanking.exception.BalanceNotSufficientException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.BeneficiaryRepository;
import com.hcl.retailbanking.repository.CustomerRepository;
import com.hcl.retailbanking.repository.TransactionRepository;

@Service
public class FundTransferServiceImpl implements FundTransferService {

	@Autowired
	AccountRepository accountRepository;
	@Autowired
	BeneficiaryRepository beneficiaryRepository;
	@Autowired
	TransactionRepository transactionHistoryRepository;
	@Autowired
	CustomerRepository customerRepository;
	/**
	 * @author Suma
	 * 
	 *         Method is used for transfering fund to beneficiary using fundTransferRequestDto
	 *          i.e, based on fromAccount,toAccount,DateTime
	 * 
	 * @param fundTransferRequestDto
	 * @return FundTransferResponseDto that includes statusCode n message
	 *  
	 * @throws BalanceNotSufficientException
	 */

	@Override
	@Transactional
	public FundTransferResponseDto transferAmount(FundTransferRequestDto fundTransferRequestDto)
			throws BalanceNotSufficientException {
		FundTransferResponseDto fundTransferResponseDto = new FundTransferResponseDto();
		if (fundTransferRequestDto.getAmount() == 0 || fundTransferRequestDto.getFromAccount() == null
				|| fundTransferRequestDto.getToAccount() == null || fundTransferRequestDto.getCustomerId() == 0) {
			throw new BalanceNotSufficientException(ApplicationConstants.INVALID_DETAILS);
		}
		if (accountRepository.findByCustomerIdAndAccountNumber(fundTransferRequestDto.getCustomerId(),
				fundTransferRequestDto.getFromAccount()) == null) {
			throw new BalanceNotSufficientException(ApplicationConstants.CUSTOMER_NOT_FOUND);
		}
		Optional<Account> account = accountRepository.findByAccountNumber(fundTransferRequestDto.getFromAccount());
		if (!account.isPresent()) {
			throw new BalanceNotSufficientException(ApplicationConstants.CUSTOMER_NOT_FOUND);
		}
		if (beneficiaryRepository.findByCustomerIdAndAccountNumber(fundTransferRequestDto.getCustomerId(),
				fundTransferRequestDto.getToAccount()) == null) {
			throw new BalanceNotSufficientException(ApplicationConstants.RECEIVER_NOT_FOUND);
		}
		Optional<Beneficiary> beneficiary = beneficiaryRepository.findByAccountNumber(fundTransferRequestDto.getToAccount());
		if (!beneficiary.isPresent()) {
			throw new BalanceNotSufficientException(ApplicationConstants.RECEIVER_NOT_FOUND);
		}
		if (fundTransferRequestDto.getFromAccount().equals(account.get().getAccountNumber()))
		{
			if (fundTransferRequestDto.getToAccount().equals(beneficiary.get().getAccountNumber()))
			{
				if (fundTransferRequestDto.getAmount() <= account.get().getBalance()) {
					TransactionDto transactionDto = new TransactionDto();
					transactionDto.setAmount(fundTransferRequestDto.getAmount());
					transactionDto.setDateTime(fundTransferRequestDto.getDate());
					transactionDto.setDescription(ApplicationConstants.DESCRIPTION);
					transactionDto.setFromAccount(fundTransferRequestDto.getFromAccount());
					transactionDto.setTransactionType(ApplicationConstants.TRANSACTION_TYPE);
					transactionDto.setToAccount(fundTransferRequestDto.getToAccount());
					Transaction transaction = new Transaction();
					BeanUtils.copyProperties(transactionDto, transaction);
					transactionHistoryRepository.save(transaction);
					Account account1 = accountRepository.findByCustomerIdAndAccountNumber(
							fundTransferRequestDto.getCustomerId(), fundTransferRequestDto.getFromAccount());
					double k = account1.getBalance();
					account1.setBalance(k - fundTransferRequestDto.getAmount());
					accountRepository.save(account1);
					Beneficiary beneficiary1 = beneficiaryRepository.findByCustomerIdAndAccountNumber(
							fundTransferRequestDto.getCustomerId(), fundTransferRequestDto.getToAccount());
					
					double l = beneficiary1.getBalance();
					beneficiary1.setBalance(l + fundTransferRequestDto.getAmount());
					beneficiaryRepository.save(beneficiary1);
					fundTransferResponseDto.setMessage(ApplicationConstants.MESSAGE);
					fundTransferResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);

				} else {
					TransactionDto transactionDto = new TransactionDto();
					transactionDto.setAmount(fundTransferRequestDto.getAmount());
					transactionDto.setDateTime(fundTransferRequestDto.getDate());
					transactionDto.setDescription(ApplicationConstants.FAILURE_DESCRIPTION);
					transactionDto.setFromAccount(fundTransferRequestDto.getFromAccount());
					transactionDto.setTransactionType(ApplicationConstants.TRANSACTION_TYPE);
					transactionDto.setToAccount(fundTransferRequestDto.getToAccount());
					Transaction transaction = new Transaction();
					BeanUtils.copyProperties(transactionDto, transaction);
					transactionHistoryRepository.save(transaction);
					fundTransferResponseDto.setMessage(ApplicationConstants.FAILURE_MESSAGE);
					fundTransferResponseDto.setStatusCode(ApplicationConstants.UNSUCCESS_CODE);
				}

			}

		}

		return fundTransferResponseDto;

	}
}
