package com.hcl.retailbanking.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.retailbanking.dto.BeneficiaryDto;
import com.hcl.retailbanking.dto.ResponseBeneficiaryDto;
import com.hcl.retailbanking.entity.Beneficiary;
import com.hcl.retailbanking.exception.BeneficiaryException;

@Service
public interface BeneficiaryService {
	
	public ResponseBeneficiaryDto addBeneficiaryService(BeneficiaryDto beneficiarydto) throws BeneficiaryException;

	List<Beneficiary> getbeneficiarydetails(int customerId) throws BeneficiaryException;

}
