package com.hcl.retailbanking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.retailbanking.constants.ApplicationConstants;
import com.hcl.retailbanking.dto.HistoryDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Transaction;
import com.hcl.retailbanking.exception.TransactionHistoryException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.TransactionRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HistoryServiceImpl implements HistoryService {

	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	AccountRepository accountRepository;

	/**
	 * @author Anuradha
	 * 
	 *         Method is used for fetching transaction history using userId i.e,
	 *         particular customer can view all his transaction history
	 * 
	 * @param userId
	 * @return List<HistoryDto> that includes fromAccount,toAccount,dateTime,
	 *         amount,transactionType n description
	 * 
	 * @throws TransactionHistoryException
	 */
	@Override
	public List<HistoryDto> history(Integer userId) throws TransactionHistoryException {
		List<HistoryDto> historyDtoList = new ArrayList<>();
		Optional<List<Account>> account = accountRepository.findByCustomerId(userId);
		if (!account.isPresent()) {
			log.info("inside if");
			throw new TransactionHistoryException(ApplicationConstants.NOUSER_FOUND);
		}
		account.get().forEach(acc -> {
			Optional<List<Transaction>> fromAccountList = transactionRepository
					.findByFromAccount(acc.getAccountNumber());
			log.info("inside from account");
			fromAccountList.get().forEach(fromlist -> {
				HistoryDto historyDto = new HistoryDto();
				historyDto.setAmount(fromlist.getAmount());
				historyDto.setDateTime(fromlist.getDateTime());
				historyDto.setDescription(fromlist.getDescription());
				historyDto.setTransactionType(fromlist.getTransactionType());
				historyDto.setFromAccount(fromlist.getFromAccount());
				historyDto.setToAccount(fromlist.getToAccount());
				historyDtoList.add(historyDto);
			});
		});
		return historyDtoList;
	}

}
