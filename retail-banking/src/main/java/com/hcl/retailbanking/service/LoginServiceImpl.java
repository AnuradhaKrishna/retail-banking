package com.hcl.retailbanking.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.retailbanking.constants.ApplicationConstants;
import com.hcl.retailbanking.dto.LoginDto;
import com.hcl.retailbanking.dto.ResponseDto;
import com.hcl.retailbanking.entity.Account;
import com.hcl.retailbanking.entity.Customer;
import com.hcl.retailbanking.exception.CustomisedException;
import com.hcl.retailbanking.repository.AccountRepository;
import com.hcl.retailbanking.repository.CustomerRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;
	/**
	 * @author Bhairavi
	 * 
	 *         Method is used login customer using loginDto
	 *          i.e, based on fromAccount,toAccount,DateTime
	 * 
	 * @param logindto
	 * @return ResponseDto that includes statusCode, customerName,amount n account no.
	 *  
	 * @throws CustomisedException
	 */
	@Override
	public ResponseDto userLogin(LoginDto loginDto) throws CustomisedException {
		ResponseDto responseDto = new ResponseDto();

		if (loginDto.getUserId() == null || loginDto.getPassword().isEmpty()) {
			responseDto.setStatusCode(ApplicationConstants.UNSUCCESS_CODE);

		} else {
			Optional<List<Customer>> customer = customerRepository.findByUserIdAndPassword(loginDto.getUserId(),
					loginDto.getPassword());
			if (!customer.isPresent()) {
				throw new CustomisedException(ApplicationConstants.CUSTOMER_NOT_REGISTERED);
			}
			Optional<List<Account>> account = accountRepository.findByCustomerId(customer.get().get(0).getCustomerId());
			if (!account.isPresent()) {
				throw new CustomisedException(ApplicationConstants.CUSTOMER_NOT_REGISTERED);
			}

			account.get().forEach(accounts -> {
				responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
				responseDto.setCustomerName(customer.get().get(0).getCustomerName());
				responseDto.setAccountNumber(accounts.getAccountNumber());
				responseDto.setBalance(accounts.getBalance());
			});

		}
		return responseDto;

	}

}
