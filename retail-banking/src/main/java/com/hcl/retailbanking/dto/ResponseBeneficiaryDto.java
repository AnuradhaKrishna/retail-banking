package com.hcl.retailbanking.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseBeneficiaryDto {

	private Integer statusCode;
	private String message;
	List<BeneficiaryDto> beneficiarydto;
}
