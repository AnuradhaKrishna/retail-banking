package com.hcl.retailbanking.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {

	private Long userId;
	private String password;

}
