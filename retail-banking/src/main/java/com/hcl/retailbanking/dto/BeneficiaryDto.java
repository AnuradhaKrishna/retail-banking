package com.hcl.retailbanking.dto;

import java.util.List;

import com.hcl.retailbanking.entity.Beneficiary;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class BeneficiaryDto {
	
	private Integer customerId;
	List<Beneficiary> beneficiarydetails;

}
