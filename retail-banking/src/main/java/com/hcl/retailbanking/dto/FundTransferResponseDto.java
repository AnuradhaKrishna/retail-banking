package com.hcl.retailbanking.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FundTransferResponseDto {

	private Integer statusCode;
	private String message;

}
