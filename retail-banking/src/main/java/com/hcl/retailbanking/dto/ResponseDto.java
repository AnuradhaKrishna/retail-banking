package com.hcl.retailbanking.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDto {

	private int statusCode;
	private String customerName;
	private Long accountNumber;
	private Double balance;

}
