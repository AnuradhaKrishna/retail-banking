package com.hcl.retailbanking.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionDto {

	private Long fromAccount;
	private Long toAccount;
	private String transactionType;
	private LocalDateTime dateTime;
	private double amount;
	private String status;
	private String description;

}
