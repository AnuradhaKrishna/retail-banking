package com.hcl.retailbanking.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FundTransferRequestDto {

	private Long fromAccount;
	private Long toAccount;
	private double amount;
	private LocalDateTime date;
	private Integer customerId;

}
