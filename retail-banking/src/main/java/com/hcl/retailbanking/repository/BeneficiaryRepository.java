package com.hcl.retailbanking.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.retailbanking.entity.Beneficiary;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Integer>{
	Optional<Beneficiary> findByAccountNumber(Long toAccount);
	Beneficiary findByCustomerIdAndAccountNumber(Integer customerId, Long toAccount);

}
