package com.hcl.retailbanking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.retailbanking.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

	Optional<List<Account>> findByCustomerId(Integer userId);

	Optional<Account> findByAccountNumber(Long fromAccount);

	Account findByCustomerIdAndAccountNumber(Integer customerId, Long fromAccount);

}
