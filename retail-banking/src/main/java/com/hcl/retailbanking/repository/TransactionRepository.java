package com.hcl.retailbanking.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.retailbanking.entity.Transaction;
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	Optional<List<Transaction>> findByFromAccount(Long accountNumber);

	Optional<Transaction> findByTransactionId(Integer transactionId);

	Optional<List<Transaction>> findByToAccount(Long accountNumber);

}
