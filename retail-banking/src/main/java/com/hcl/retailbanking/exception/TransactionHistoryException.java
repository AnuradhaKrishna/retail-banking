package com.hcl.retailbanking.exception;

public class TransactionHistoryException extends Exception {
	private static final long serialVersionUID = 1L;

	public TransactionHistoryException(String exception) {

		super(exception);
	}

}
