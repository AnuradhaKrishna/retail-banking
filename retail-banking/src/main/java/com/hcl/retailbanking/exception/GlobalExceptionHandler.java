package com.hcl.retailbanking.exception;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hcl.retailbanking.constants.ApplicationConstants;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		Map<String, Object> body = new LinkedHashMap<>();
		body.put("timestamp", new Date());
		body.put("status", status.value());
		// Get all errors for field valid
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		body.put("errors", errors);
		return new ResponseEntity<>(body, headers, status);
	}

	@ExceptionHandler(CustomisedException.class)
	public ResponseEntity<ErrorMessage> error(CustomisedException ex) {
		ErrorMessage er = new ErrorMessage();
		er.setMessage(ex.getMessage());
		er.setStatus(HttpStatus.OK.value());
		return new ResponseEntity<>(er, HttpStatus.OK);
	}

	@ExceptionHandler(BeneficiaryException.class)
	ResponseEntity<ErrorMessage> beneficiaryException(BeneficiaryException beneficiaryException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(beneficiaryException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(TransactionHistoryException.class)
	ResponseEntity<ErrorMessage> transactionHistoryException(TransactionHistoryException transactionHistoryException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(transactionHistoryException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(BalanceNotSufficientException.class)
	ResponseEntity<ErrorMessage> balanceNotSufficientException(BalanceNotSufficientException balanceNotSufficientException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(balanceNotSufficientException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}
	

}
