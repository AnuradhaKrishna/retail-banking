package com.hcl.retailbanking.exception;

public class CustomisedException extends Exception {

	private static final long serialVersionUID = 1L;

	public CustomisedException(String exception) {

		super(exception);
	}

}
