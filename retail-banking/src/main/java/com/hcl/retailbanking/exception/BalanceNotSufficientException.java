package com.hcl.retailbanking.exception;

public class BalanceNotSufficientException extends Exception {

	private static final long serialVersionUID = 1L;

	public BalanceNotSufficientException(String exception) {

		super(exception);
	}

}
