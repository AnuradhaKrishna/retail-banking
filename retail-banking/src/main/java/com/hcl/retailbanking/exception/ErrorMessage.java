package com.hcl.retailbanking.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorMessage {

	private String message;
	private int status;

}
