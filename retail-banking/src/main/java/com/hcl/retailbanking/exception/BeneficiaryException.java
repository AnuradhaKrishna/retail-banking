package com.hcl.retailbanking.exception;

public class BeneficiaryException extends Exception {

	private static final long serialVersionUID = 1L;

	public BeneficiaryException(String exception) {

		super(exception);
	}

}
